import logging

from sqlalchemy import Column, BigInteger
from sqlalchemy import ForeignKey
from sqlalchemy import Integer, String
from vortex.Tuple import Tuple, addTupleType

from peek_abstract_chunked_index.private.tuples.ACIProcessorQueueTupleABC import (
    ACIProcessorQueueTupleABC,
)
from peek_plugin_index_blueprint._private.PluginNames import indexBlueprintTuplePrefix
from .DeclarativeBase import DeclarativeBase

logger = logging.getLogger(__name__)


@addTupleType
class ThingIndexCompilerQueue(Tuple, DeclarativeBase, ACIProcessorQueueTupleABC):
    __tablename__ = "ThingIndexCompilerQueue"
    __tupleType__ = indexBlueprintTuplePrefix + "ThingIndexCompilerQueueTable"

    id = Column(BigInteger, primary_key=True, autoincrement=True)

    modelSetId = Column(
        Integer,
        ForeignKey("ModelSet.id", ondelete="CASCADE"),
        nullable=False,
        autoincrement=True,
    )

    chunkKey = Column(String, primary_key=True)

    @classmethod
    def sqlCoreLoad(cls, row):
        return ThingIndexCompilerQueue(
            id=row.id, modelSetId=row.modelSetId, chunkKey=row.chunkKey
        )

    @property
    def ckiUniqueKey(self):
        return self.chunkKey
