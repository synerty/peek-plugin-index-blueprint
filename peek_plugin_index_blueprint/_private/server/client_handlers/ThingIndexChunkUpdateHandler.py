import logging
from typing import Dict

from peek_abstract_chunked_index.private.server.client_handlers.ACIChunkUpdateHandlerABC import (
    ACIChunkUpdateHandlerABC,
)
from peek_abstract_chunked_index.private.tuples.ACIEncodedChunkTupleABC import (
    ACIEncodedChunkTupleABC,
)
from peek_plugin_index_blueprint._private.client.controller.ThingIndexCacheController import (
    clientThingIndexUpdateFromServerFilt,
)
from peek_plugin_index_blueprint._private.storage.ThingIndexEncodedChunk import (
    ThingIndexEncodedChunk,
)

logger = logging.getLogger(__name__)


class ThingIndexChunkUpdateHandler(ACIChunkUpdateHandlerABC):
    _ChunkedTuple: ACIEncodedChunkTupleABC = ThingIndexEncodedChunk
    _updateFromServerFilt: Dict = clientThingIndexUpdateFromServerFilt
    _logger: logging.Logger = logger
