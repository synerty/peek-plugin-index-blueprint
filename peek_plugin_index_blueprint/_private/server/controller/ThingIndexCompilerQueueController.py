import logging

from peek_abstract_chunked_index.private.server.controller.ACIProcessorQueueControllerABC import (
    ACIProcessorQueueControllerABC,
    ACIProcessorQueueBlockItem,
)
from peek_abstract_chunked_index.private.server.controller.ACIProcessorStatusNotifierABC import (
    ACIProcessorStatusNotifierABC,
)
from peek_abstract_chunked_index.private.tuples.ACIProcessorQueueTupleABC import (
    ACIProcessorQueueTupleABC,
)
from peek_plugin_index_blueprint._private.server.client_handlers.ThingIndexChunkUpdateHandler import (
    ThingIndexChunkUpdateHandler,
)
from peek_plugin_index_blueprint._private.server.controller.ThingIndexStatusController import (
    ThingIndexStatusController,
)
from peek_plugin_index_blueprint._private.storage.ThingIndex import ThingIndex
from peek_plugin_index_blueprint._private.storage.ThingIndexCompilerQueue import (
    ThingIndexCompilerQueue,
)
from peek_plugin_index_blueprint._private.storage.ThingIndexEncodedChunk import (
    ThingIndexEncodedChunk,
)

logger = logging.getLogger(__name__)


class _Notifier(ACIProcessorStatusNotifierABC):
    def __init__(self, adminStatusController: ThingIndexStatusController):
        self._adminStatusController = adminStatusController

    def setProcessorStatus(self, state: bool, queueSize: int):
        self._adminStatusController.status.thingIndexCompilerQueueStatus = state
        self._adminStatusController.status.thingIndexCompilerQueueSize = queueSize
        self._adminStatusController.notify()

    def addToProcessorTotal(self, delta: int):
        self._adminStatusController.status.thingIndexCompilerQueueProcessedTotal += (
            delta
        )
        self._adminStatusController.notify()

    def setProcessorError(self, error: str):
        self._adminStatusController.status.thingIndexCompilerQueueLastError = error
        self._adminStatusController.notify()


class ThingIndexCompilerQueueController(ACIProcessorQueueControllerABC):
    QUEUE_ITEMS_PER_TASK = 10
    POLL_PERIOD_SECONDS = 1.000

    QUEUE_BLOCKS_MAX = 20
    QUEUE_BLOCKS_MIN = 4

    WORKER_TASK_TIMEOUT = 60.0

    _logger = logger
    _QueueDeclarative: ACIProcessorQueueTupleABC = ThingIndexCompilerQueue
    _VacuumDeclaratives = (ThingIndexCompilerQueue, ThingIndex, ThingIndexEncodedChunk)

    def __init__(
        self,
        dbSessionCreator,
        statusController: ThingIndexStatusController,
        clientUpdateHandler: ThingIndexChunkUpdateHandler,
    ):
        ACIProcessorQueueControllerABC.__init__(
            self, dbSessionCreator, _Notifier(statusController)
        )

        self._clientUpdateHandler: ThingIndexChunkUpdateHandler = clientUpdateHandler

    def _sendToWorker(self, block: ACIProcessorQueueBlockItem):
        from peek_plugin_index_blueprint._private.worker.tasks.ThingIndexCompiler import (
            compileThingIndexChunk,
        )

        return compileThingIndexChunk.delay(block.itemsEncodedPayload)

    def _processWorkerResults(self, results):
        self._clientUpdateHandler.sendChunks(results)

    def _dedupeQueueSql(self, lastFetchedId: int, dedupeLimit: int):
        return """
                 with sq_raw as (
                    SELECT "id", "chunkKey"
                    FROM pl_index_blueprint."ThingIndexCompilerQueue"
                    WHERE id > %(id)s
                    LIMIT %(limit)s
                ), sq as (
                    SELECT min(id) as "minId", "chunkKey"
                    FROM sq_raw
                    GROUP BY  "chunkKey"
                    HAVING count("chunkKey") > 1
                )
                DELETE
                FROM pl_index_blueprint."ThingIndexCompilerQueue"
                     USING sq sq1
                WHERE pl_index_blueprint."ThingIndexCompilerQueue"."id" != sq1."minId"
                    AND pl_index_blueprint."ThingIndexCompilerQueue"."id" > %(id)s
                    AND pl_index_blueprint."ThingIndexCompilerQueue"."chunkKey" = sq1."chunkKey"

            """ % {
            "id": lastFetchedId,
            "limit": dedupeLimit,
        }
