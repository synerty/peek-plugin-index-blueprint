import logging

from peek_abstract_chunked_index.private.server.controller.ACIProcessorStatusControllerABC import (
    ACIProcessorStatusControllerABC,
)
from peek_plugin_index_blueprint._private.tuples.ThingIndexServerStatusTuple import (
    ThingIndexServerStatusTuple,
)

logger = logging.getLogger(__name__)


class ThingIndexStatusController(ACIProcessorStatusControllerABC):
    NOTIFY_PERIOD = 2.0

    _StateTuple = ThingIndexServerStatusTuple
