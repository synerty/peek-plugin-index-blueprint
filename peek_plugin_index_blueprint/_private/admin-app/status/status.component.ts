import { takeUntil } from "rxjs/operators";
import { Component } from "@angular/core";
import {
    NgLifeCycleEvents,
    TupleDataObserverService,
    TupleSelector,
} from "@synerty/vortexjs";
import { ThingIndexServerStatusTuple } from "@peek/peek_plugin_index_blueprint/_private";
import { BalloonMsgService } from "@synerty/peek-plugin-base-js";

@Component({
    selector: "pl-index-blueprint-status",
    templateUrl: "./status.component.html",
})
export class StatusComponent extends NgLifeCycleEvents {
    item: ThingIndexServerStatusTuple = new ThingIndexServerStatusTuple();

    constructor(
        private balloonMsg: BalloonMsgService,
        private tupleObserver: TupleDataObserverService
    ) {
        super();

        let ts = new TupleSelector(ThingIndexServerStatusTuple.tupleName, {});
        this.tupleObserver
            .subscribeToTupleSelector(ts)
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((tuples: ThingIndexServerStatusTuple[]) => {
                this.item = tuples[0];
            });
    }
}
