import { Component } from "@angular/core";
import { BalloonMsgService } from "@synerty/peek-plugin-base-js";
import {
    extend,
    NgLifeCycleEvents,
    TupleDataObserverService,
    TupleLoader,
    VortexService,
} from "@synerty/vortexjs";
import { indexBlueprintFilt } from "@peek/peek_plugin_index_blueprint/_private";
import {
    IndexBlueprintModelSetTuple,
    ThingTypeTuple,
} from "@peek/peek_plugin_index_blueprint";

@Component({
    selector: "pl-index-blueprint-edit-thing-type",
    templateUrl: "./edit.component.html",
})
export class EditThingTypeComponent extends NgLifeCycleEvents {
    items: ThingTypeTuple[] = [];
    loader: TupleLoader;
    modelSetById: { [id: number]: IndexBlueprintModelSetTuple } = {};
    thingTypeById: { [id: number]: ThingTypeTuple } = {};
    // This must match the dict defined in the admin_backend handler
    private readonly filt = {
        key: "admin.Edit.ThingType",
    };

    constructor(
        private balloonMsg: BalloonMsgService,
        vortexService: VortexService,
        private tupleObserver: TupleDataObserverService
    ) {
        super();

        this.loader = vortexService.createTupleLoader(this, () =>
            extend({}, this.filt, indexBlueprintFilt)
        );

        this.loader.observable.subscribe(
            (tuples: ThingTypeTuple[]) => (this.items = tuples)
        );

        // let modelSetTs = new TupleSelector(ModelSetTuple.tupleName, {});
        // this.tupleObserver.subscribeToTupleSelector(modelSetTs)
        //     .pipe(takeUntil(this.onDestroyEvent))
        //     .subscribe((tuples: ModelSetTuple[]) => {
        //         this.modelSetById = {};
        //         for (let tuple of tuples) {
        //             this.modelSetById[tuple.id] = tuple;
        //         }
        //     });
        //
        // let thingTypeTs = new TupleSelector(ThingType.tupleName, {});
        // this.tupleObserver.subscribeToTupleSelector(thingTypeTs)
        //     .pipe(takeUntil(this.onDestroyEvent))
        //     .subscribe((tuples: ThingType[]) => {
        //         this.thingTypeById = {};
        //         for (let tuple of tuples) {
        //             this.thingTypeById[tuple.id] = tuple;
        //         }
        //     });
    }

    modelSetTitle(tuple: ThingTypeTuple): string {
        // let modelSet = this.modelSetById[tuple.modelSetId];
        // return modelSet == null ? "" : modelSet.name;
        return "TODO";
    }

    thingTypeTitle(tuple: ThingTypeTuple): string {
        // let thingType = this.thingTypeById[tuple.thing];
        // return thingType == null ? "" : thingType.name;
        return "TODO";
    }

    save() {
        this.loader
            .save()
            .then(() => this.balloonMsg.showSuccess("Save Successful"))
            .catch((e) => this.balloonMsg.showError(e));
    }

    resetClicked() {
        this.loader
            .load()
            .then(() => this.balloonMsg.showSuccess("Reset Successful"))
            .catch((e) => this.balloonMsg.showError(e));
    }
}
