import { takeUntil } from "rxjs/operators";
import { Component } from "@angular/core";
import { NgLifeCycleEvents } from "@synerty/vortexjs";
import { IndexBlueprintTupleService } from "@peek/peek_plugin_index_blueprint/_private";
import {
    ThingIndexLoaderService,
    ThingIndexLoaderStatusTuple,
} from "@peek/peek_plugin_index_blueprint/_private/thing-index-loader";

@Component({
    selector: "peek-plugin-index-blueprint-cfg",
    templateUrl: "index-blueprint-cfg.component.web.html",
})
export class ThingIndexCfgComponent extends NgLifeCycleEvents {
    thingIndexStatus: ThingIndexLoaderStatusTuple =
        new ThingIndexLoaderStatusTuple();

    constructor(
        private thingIndexLoader: ThingIndexLoaderService,
        private tupleService: IndexBlueprintTupleService
    ) {
        super();

        this.thingIndexStatus = this.thingIndexLoader.status();
        this.thingIndexLoader
            .statusObservable()
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((value) => (this.thingIndexStatus = value));
    }
}
