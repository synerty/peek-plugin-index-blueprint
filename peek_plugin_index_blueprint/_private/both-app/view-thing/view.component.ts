import { takeUntil } from "rxjs/operators";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { BalloonMsgService, HeaderService } from "@synerty/peek-plugin-base-js";
import { NgLifeCycleEvents, VortexStatusService } from "@synerty/vortexjs";
import {
    IndexBlueprintService,
    ThingIndexResultI,
    ThingTuple,
} from "@peek/peek_plugin_index_blueprint";

@Component({
    selector: "plugin-index-blueprint-result",
    templateUrl: "view.component.web.html",
})
export class ViewThingComponent extends NgLifeCycleEvents implements OnInit {
    thing: ThingTuple = null;
    thingTypeName: string = "";
    modelSetKey: string = "";
    key: string = "";

    constructor(
        private balloonMsg: BalloonMsgService,
        private route: ActivatedRoute,
        private indexBlueprintService: IndexBlueprintService,
        private vortexStatus: VortexStatusService,
        private headerService: HeaderService
    ) {
        super();

        headerService.setTitle("View Thing");
    }

    ngOnInit() {
        this.route.params
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((params: Params) => {
                let vars = {};

                if (typeof window !== "undefined") {
                    window.location.href.replace(
                        /[?&]+([^=&]+)=([^&]*)/gi,
                        (m, key, value) => (vars[key] = value)
                    );
                }

                this.key = params["key"] || vars["key"];
                this.modelSetKey = params["modelSetKey"] || vars["modelSetKey"];

                if (
                    !(
                        this.modelSetKey &&
                        this.modelSetKey.length &&
                        this.key &&
                        this.key.length
                    )
                )
                    return;

                this.headerService.setTitle(`Loading Thing ${this.key}`);
                this.indexBlueprintService
                    .getThings(this.modelSetKey, [this.key])
                    .then((things: ThingIndexResultI) => {
                        this.loadThing(things[this.key], this.key);
                    })
                    .catch((e) => this.balloonMsg.showError(e));
            });
    }

    find(): void {
        if (!(this.modelSetKey && this.modelSetKey.length)) {
            this.balloonMsg.showWarning("Please set modelSetKey");
            return;
        }

        if (!(this.key && this.key.length)) {
            this.balloonMsg.showWarning("Please set key");
            return;
        }

        this.indexBlueprintService
            .getThings(this.modelSetKey, [this.key])
            .then((things: ThingIndexResultI) => {
                this.loadThing(things[this.key], this.key);
            })
            .catch((e) => this.balloonMsg.showError(e));
    }

    private loadThing(thing: ThingTuple, key: string) {
        this.thing = thing;
        this.thingTypeName = "";

        if (this.thing == null || this.thing.key == null) {
            this.balloonMsg.showWarning(`Failed to find ${key}`);
            this.headerService.setTitle(`Thing ${key} Not Found`);
            return;
        }
        this.balloonMsg.showSuccess(`We've found ${key}`);

        this.headerService.setTitle(`Thing ${key}`);

        this.thingTypeName = this.thing.thingType.name;
    }
}
