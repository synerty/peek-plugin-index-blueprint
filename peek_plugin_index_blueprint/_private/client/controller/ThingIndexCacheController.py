import logging

from peek_abstract_chunked_index.private.client.controller.ACICacheControllerABC import (
    ACICacheControllerABC,
)
from peek_plugin_index_blueprint._private.PluginNames import indexBlueprintFilt
from peek_plugin_index_blueprint._private.server.client_handlers.ThingIndexChunkLoadRpc import (
    ThingIndexChunkLoadRpc,
)
from peek_plugin_index_blueprint._private.storage.ThingIndexEncodedChunk import (
    ThingIndexEncodedChunk,
)

logger = logging.getLogger(__name__)

clientThingIndexUpdateFromServerFilt = dict(key="clientThingIndexUpdateFromServer")
clientThingIndexUpdateFromServerFilt.update(indexBlueprintFilt)


class ThingIndexCacheController(ACICacheControllerABC):
    """ThingIndex Cache Controller

    The ThingIndex cache controller stores all the chunks in memory,
    allowing fast access from the mobile and desktop devices.

    """

    _ChunkedTuple = ThingIndexEncodedChunk
    _chunkLoadRpcMethod = ThingIndexChunkLoadRpc.loadThingIndexChunks
    _updateFromServerFilt = clientThingIndexUpdateFromServerFilt
    _logger = logger
