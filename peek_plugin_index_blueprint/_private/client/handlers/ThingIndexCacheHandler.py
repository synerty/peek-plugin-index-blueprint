import logging
from typing import Dict

from peek_abstract_chunked_index.private.client.handlers.ACICacheHandlerABC import (
    ACICacheHandlerABC,
)
from peek_abstract_chunked_index.private.tuples.ACIUpdateDateTupleABC import (
    ACIUpdateDateTupleABC,
)
from peek_plugin_index_blueprint._private.PluginNames import indexBlueprintFilt
from peek_plugin_index_blueprint._private.tuples.ThingIndexUpdateDateTuple import (
    ThingIndexUpdateDateTuple,
)

logger = logging.getLogger(__name__)

clientThingIndexWatchUpdateFromDeviceFilt = {
    "key": "clientThingIndexWatchUpdateFromDevice"
}
clientThingIndexWatchUpdateFromDeviceFilt.update(indexBlueprintFilt)


# ModelSet HANDLER
class ThingIndexCacheHandler(ACICacheHandlerABC):
    _UpdateDateTuple: ACIUpdateDateTupleABC = ThingIndexUpdateDateTuple
    _updateFromServerFilt: Dict = clientThingIndexWatchUpdateFromDeviceFilt
