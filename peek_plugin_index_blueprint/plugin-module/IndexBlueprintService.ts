import { Injectable } from "@angular/core";
import { NgLifeCycleEvents } from "@synerty/vortexjs";
import {
    ThingIndexLoaderService,
    ThingIndexResultI,
} from "./_private/thing-index-loader";

// ----------------------------------------------------------------------------
/** ThingIndex Cache
 *
 * This class has the following responsibilities:
 *
 * 1) Maintain a local storage of the index
 *
 * 2) Return DispKey locations based on the index.
 *
 */
@Injectable()
export class IndexBlueprintService extends NgLifeCycleEvents {
    constructor(private thingIndexLoader: ThingIndexLoaderService) {
        super();
    }

    /** Get Things
     *
     * Get the objects for key from the index..
     *
     */
    getThings(modelSetKey: string, keys: string[]): Promise<ThingIndexResultI> {
        return this.thingIndexLoader.getThings(modelSetKey, keys);
    }
}
