import { addTupleType, Tuple } from "@synerty/vortexjs";
import { indexBlueprintTuplePrefix } from "../PluginNames";

@addTupleType
export class ThingIndexUpdateDateTuple extends Tuple {
    public static readonly tupleName =
        indexBlueprintTuplePrefix + "ThingIndexUpdateDateTuple";
    initialLoadComplete: boolean = false;
    updateDateByChunkKey: {} = {};
    // Improve performance of the JSON serialisation
    protected _rawJonableFields = [
        "initialLoadComplete",
        "updateDateByChunkKey",
    ];

    constructor() {
        super(ThingIndexUpdateDateTuple.tupleName);
    }
}
