export let indexBlueprintFilt = { plugin: "peek_plugin_index_blueprint" };
export let indexBlueprintTuplePrefix = "peek_plugin_index_blueprint.";

export let indexBlueprintObservableName = "peek_plugin_index_blueprint";
export let indexBlueprintActionProcessorName = "peek_plugin_index_blueprint";
export let indexBlueprintTupleOfflineServiceName =
    "peek_plugin_index_blueprint";

export let indexBlueprintBaseUrl = "peek_plugin_index_blueprint";

export let indexBlueprintCacheStorageName = "peek_plugin_index_blueprint.cache";
