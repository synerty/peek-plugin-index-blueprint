For From DocDB
--------------

Changes made:
* Rename to IndexBlueprint and ThingIndex
* Remove features specific to DocDB (The PropertyType)
* Remove non-index features (the Settings)


Fixes made:

* DELETE  _private/server/tuple_providers/ThingTupleProvider.py
* CLEANUP ThingTupleProvider from _private/server/TupleDataObservable.py
* RENAME thingIndex in /_private/server/LogicEntryHook.py
* RENAME EncodedThingIndexChunkTuple TO ThingIndexEncodedChunkTuple

